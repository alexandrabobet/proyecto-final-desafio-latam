#!/usr/bin/env python
# coding: utf-8

# In[7]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
# seaborn
import seaborn as sns
import warnings
##plt.style.use('ggplot')
import scipy.stats as stats
from scipy.stats import norm

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report



# Función para graficar histogramas
def plot_hist(dataframe, variable):
    """
    Definición: Esta función genera un histograma y grafica la media y mediana
    Input: dataframe: recibe un dataframe, y la variable, es la variable numérica a representar
    Outpout: genera un histograma
    
    """
    plt.hist(dataframe[variable].dropna(), alpha=1, color='gray', label=variable)
    plt.axvline(dataframe[variable].dropna().mean(), lw=3, color='tomato', label='Media')
    plt.axvline(dataframe[variable].dropna().median(), lw=3, color='green', label='Mediana')
    plt.title('Histograma de '+str(variable))
    plt.legend()
    plt.show();

def test_normal_distribution(dataframe, var):
    """
    Definición: Función que permite visualizar histograma de frecuencias, gráficos de residuos, curtosis
    y asimetría para definir si la variable continua pertenece a una distribución normal
    
    Input: recibe un dataframe y una variable numérica. 
    
    Outpout: imprime los gráficos y coeficientes descritos.
    
    
    """
    plt.rcParams['figure.figsize'] = (20, 5)
    sns.distplot(dataframe[var].dropna(), fit = norm);
    plt.figure()
    stats.probplot(dataframe[var].dropna(), plot = plt)
    plt.figure()
    asimetria=dataframe[var].dropna().skew().round(3)
    if asimetria >= -0.6 and asimetria<= 0.6:
        print("El Coeficiente de asimetría de "+str(var)+" tiene un valor de: ", asimetria," lo que significa la curva es simétrica y se aproxima a la normal")
    else:
        print("El coeficiente de "+str(var)+" de asimetría es: ", asimetria, " y no se ajusta  a una distribución normal")
    curtosis=dataframe[var].dropna().kurt().round(3)
    if curtosis >= -0.6 and curtosis<= 0.6:
        print("El Coeficiente  de "+str(var)+" de curtosis tiene un valor de: ", curtosis,"lo que significa la curva es mesocurtica y se aproxima a la normal")
    else:
        print("El coeficiente de curtosis  de "+str(var)+" es: ",curtosis ," y no se ajusta  a una distribución normal");
        
        
def graficar_dotplot(dataframe, plot_var, plot_by, global_stat = False, statistic = 'mean'):
    plt.grid()
    plt.xlabel(plot_var)
    plt.ylabel(plot_by)
    if statistic == 'mean':
        plot = dataframe.groupby(plot_by)[plot_var].mean()
    elif statistic == 'median':
        plot = dataframe.groupby(plot_by)[plot_var].median()
    plt.plot(plot.values, plot.index, 'x')
    if global_stat:
        if statistic == 'mean':
            plt.axvline(dataframe[plot_var].mean(), color='green', linestyle='-')
            print(dataframe[plot_var].mean(), dataframe[plot_var].median())
        elif statistic == 'median':
            plt.axvline(dataframe[plot_var].median(), color='green', linestyle='-');
            
            
            
# Función para obtener las correlaciones de Pearson del vector en comparación al resto del dataframe
def fetch_features(dataframe, vector_objetivo='medv'):
    """
    Definición:Función para obtener las correlaciones de Pearson del vector en comparación al resto del dataframe
    Input:dataframe de origen y vector_objetivo='medv', para este caso por defecto es el valor mediano de las propiedades
    Outpout: retorna un dataframe con el valor de correlación de pearson y la correlación absoluta, ordenadas de manera descendente
    """
    columns = dataframe.columns
    attr_name = []
    pearson_r = []
    abs_pearson_r = []
    for col in columns:
        if col != vector_objetivo:
            attr_name.append(col)
            pearson_r.append(dataframe[col].corr(dataframe[vector_objetivo]))
            abs_pearson_r.append(abs(dataframe[col].corr(dataframe[vector_objetivo])))
            features = pd.DataFrame({'attribute':attr_name, 'corr':pearson_r,'abs_corr':abs_pearson_r})
            features = features.set_index('attribute')
    return  features.sort_values(by=['abs_corr'], ascending=False);  


def identify_high_correlations(df, threshold=.7):
    """
    identify_high_correlations: Genera un reporte sobre las correlaciones existentes entre variables, condicional a un nivel arbitrario.

    Parámetros de ingreso:
        - df: un objeto pd.DataFrame, por lo general es la base de datos a trabajar.
        - threshold: Nivel de correlaciones a considerar como altas. Por defecto es .7.

    Retorno:
        - Un pd.DataFrame con los nombres de las variables y sus correlaciones
    """

    # extraemos la matriz de correlación con una máscara booleana
    tmp = df.corr().mask(abs(df.corr()) < .7, df)
    # convertimos a long format
    tmp = pd.melt(tmp)
    # agregamos una columna extra que nos facilitará los cruces entre variables
    tmp['var2'] = list(df.columns) * len(df.columns)
    # reordenamos
    tmp = tmp[['variable', 'var2', 'value']].dropna()
    # eliminamos valores duplicados
    tmp = tmp[tmp['value'].duplicated()]
    # eliminamos variables con valores de 1 
    return tmp[tmp['value'] < 1.00]

def mostrar_resultados(y_test, pred_y):
    conf_matrix = confusion_matrix(y_test, pred_y)
    plt.figure(figsize=(16, 6))
    sns.heatmap(conf_matrix, xticklabels='', yticklabels='', annot=True,center=0, cmap='Blues_r',  fmt='.1f');
    plt.title("Confusion matrix")
    plt.ylabel('True class')
    plt.xlabel('Predicted class')
    plt.show()
    print (classification_report(y_test, pred_y))

       


# In[ ]:




